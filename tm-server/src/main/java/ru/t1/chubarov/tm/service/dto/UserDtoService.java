package ru.t1.chubarov.tm.service.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.chubarov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.dto.IProjectDtoService;
import ru.t1.chubarov.tm.api.service.dto.ITaskDtoService;
import ru.t1.chubarov.tm.api.service.dto.IUserDtoService;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.repository.dto.UserDtoRepository;
import ru.t1.chubarov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Getter
@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserDtoService implements IUserDtoService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public IUserDtoRepository getUserRepository() {
        return this.context.getBean(UserDtoRepository.class);
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL.toString());
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException(login, email);
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL.toString());
        @NotNull IUserDtoRepository repository = getUserRepository();
        @NotNull EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role.toString());
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.isLoginExist(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.isEmailExist(email);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            UserDTO users = repository.findByLogin(login);
            return users;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final UserDTO model = repository.findOneById(id);
            if (model == null) throw new UserNotFoundException();
            return model;
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO removeOne(@Nullable final UserDTO model) throws Exception {
        if (model == null) return null;
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final String userId = model.getId();
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
            taskService.removeAll(userId);
            projectService.removeAll(userId);
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public UserDTO removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return null;
        return removeOne(user);
    }

    @NotNull
    @Override
    public UserDTO removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException("login", email);
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) return null;
        return removeOne(user);
    }

    @NotNull
    @Override
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO updateUser(@Nullable final String id,
                              @Nullable final String firstName,
                              @Nullable final String lastName,
                              @Nullable final String middleName) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final UserDTO user = findOneById(id);
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<UserDTO> set(@NotNull Collection<UserDTO> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            clear();
            for (@NotNull final UserDTO user : models) {
                repository.add(user);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Override
    public void clear() {
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        }finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final IUserDtoRepository repository = getUserRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        }finally {
            entityManager.close();
        }
    }

}
